// ignore_for_file: prefer_const_constructors, duplicate_ignore
import 'package:demo2/registration.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Authentication extends StatefulWidget {
  Authentication({Key? key}) : super(key: key);

  @override
  State<Authentication> createState() => _AuthenticationState();
}

class _AuthenticationState extends State<Authentication> {
  TextEditingController emailfield = TextEditingController();
  TextEditingController passwordfield = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.redAccent,
        elevation: 1,
        toolbarHeight: 0.5,
      ),
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextFormField(
              controller: emailfield,
              decoration: InputDecoration(
                labelText: "email",
                hintText: "khaled@gmail.com",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              textInputAction: TextInputAction.next,
              validator: (value) {
                if (value!.isEmpty) {
                  return ("Please Enter Your Email");
                }
                if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                    .hasMatch(value)) {
                  return "Please Enter a valid email";
                }
                return null;
              },
            ),
            TextFormField(
              controller: passwordfield,
              obscureText: true,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                labelText: "password",
                hintText: "enter you password here",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              validator: (value) {
                RegExp regex = new RegExp(r'^.{6,}$');
                if (value!.isEmpty) {
                  return ("Password is required for sign up");
                }
                if (!regex.hasMatch(value)) {
                  emailfield.clear;
                  return ("Enter Valid Password(Min. 6 Character)");
                }
                return null;
              },
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              height: 45,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.redAccent,
              ),
              child: MaterialButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => Registration()));
                },
                child: Text("Sign Up"),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.4,
              height: 45,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.redAccent,
              ),
              child: MaterialButton(
                onPressed: () {
                  Fluttertoast.showToast(
                    msg: 'logged in successfully',
                    toastLength: Toast.LENGTH_SHORT,
                  );
                },
                child: Text("Log In"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

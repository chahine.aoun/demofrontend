import 'package:demo2/registration.dart';
import 'package:flutter/material.dart';

import 'authentication.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Authentication(),
    );
  }
}
//flutter run --no-sound-null-safety

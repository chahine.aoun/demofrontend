import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class Registration extends StatefulWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  final _formKey = GlobalKey<FormState>();

  final username = new TextEditingController();
  final phone = new TextEditingController();
  final email = new TextEditingController();
  final password = new TextEditingController();
  final confirmPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    //first name field
    final usern = TextFormField(
        autofocus: false,
        controller: username,
        keyboardType: TextInputType.name,
        validator: (value) {
          RegExp regex = new RegExp(r'^.{3,}$');
          if (value!.isEmpty) {
            return ("Please enter your username");
          }
          if (!regex.hasMatch(value)) {
            return ("Enter a valid username");
          }
          return null;
        },
        onSaved: (value) {
          username.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          labelText: "Username",
          hintText: "name.familyname",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));
    final number = IntlPhoneField(
      controller: phone,
      decoration: InputDecoration(
        labelText: "Phone Number",
        hintText: "25 555 555",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      initialCountryCode: 'TN',
      onChanged: (phone) {
        print(phone.completeNumber);
      },
    );

    //  final phonen = TextFormField(
    //   controller: phone,
    //  keyboardType:
//TextInputType.numberWithOptions(signed: false, decimal: false),
    // validator: (value) {
    //   if (value!.isEmpty) {
    //     return ("phone number cannot be empty");
    //   }
    //    return null;
    //  },
    // onSaved: (value) {
    //    phone.text = value!;
    //   },
    //    textInputAction: TextInputAction.next,
    //   );

    final emailField = TextFormField(
        autofocus: false,
        controller: email,
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          if (value!.isEmpty) {
            return ("Please Enter Your Email");
          }
          if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
              .hasMatch(value)) {
            return "Please Enter a valid email";
          }
          return null;
        },
        onSaved: (value) {
          email.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          labelText: "email",
          hintText: "khalil@gmail.com",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));
    final passwordField = TextFormField(
        autofocus: false,
        controller: password,
        obscureText: true,
        validator: (value) {
          RegExp regex = new RegExp(r'^.{6,}$');
          if (value!.isEmpty) {
            return ("Password is required for sign up");
          }
          if (!regex.hasMatch(value)) {
            email.clear;
            return ("Enter Valid Password(Min. 6 Character)");
          }
          return null;
        },
        onSaved: (value) {
          password.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          labelText: "Password",
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));
    final confirmPasswordField = TextFormField(
        autofocus: false,
        controller: confirmPassword,
        obscureText: true,
        validator: (value) {
          if (confirmPassword.text != password.text) {
            return "passwords dont match";
          }
          return null;
        },
        onSaved: (value) {
          confirmPassword.text = value!;
        },
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          labelText: "Confirm Password",
          hintText: "Confirm Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.redAccent,
        elevation: 1,
        toolbarHeight: 0.5,
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              usern,
              number,
              emailField,
              passwordField,
              confirmPasswordField,
              Container(
                width: MediaQuery.of(context).size.width / 1.4,
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.redAccent,
                ),
                child: MaterialButton(
                  onPressed: () {},
                  child: Text("Create Account"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
